# Nexa Android Template

This template is a simple showcase of how to use Nexa blockchain by importing [libnexakotlin](https://gitlab.com/nexa/libnexakotlin) ([docs](https://nexa.gitlab.io/libnexakotlin/)) in an Android Kotlin project

The purpose is to:
* Document setting up and running `libnexakotlin` in an Android project
* Serve as a template for using `libnexakotlin` in an Android project
* Document atomic code-snippets for various uses-cases of `libnexakotlin`

We are happy to provide more code example for `libnexakotlin`; join the [nexa-dev Matrix chat](https://matrix.to/#/#nexa-dev:matrix.org) to request more examples and additions to this template repo.

## Prerequisites

Android project

## Set up and run `libnexakotlin`

Steps for setting up dependencies for `libnexakotlin`

To this step: Clone this repo and open it in Android Studio 

### settings.gradle.kts:

Add libnexakotlin and mpthreads (co-dependency) repositories
```kotlin
dependencyResolutionManagement {
    // …
    repositories {
        // …
        maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
        maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") }  // mpthreads
    }
}
```

### Co-dependencies 
In app-level `build-gradle.kts` add the `libnexakotlin`, `mpthreads` and `bignum` co-dependencies

```kotlin
dependencies {
    // …
    implementation("org.nexa:libnexakotlin:0.1.80”)
    implementation("org.nexa:mpthreads:0.2.7")
    // for bigintegers
    implementation("com.ionspin.kotlin:bignum:0.3.9")
    implementation("com.ionspin.kotlin:bignum-serialization-kotlinx:0.3.9")
}
```

Set minSdkVersion to 29 or higher

```kotlin
android {
    // …
    defaultConfig {
        // …
        minSdk = 29
    }
}
```

### Initializing `libnexakotlin`

in the Android `Application()` add `initializeLibNexa` in the init function

```kotlin
package org.bitcoinunlimited.nexaandroidtemplate

import android.app.Application
import org.nexa.libnexakotlin.initializeLibNexa

class NexaTemplateApp: Application() {
    init {
        initializeLibNexa()
    }
}
```

## Examples

For educational purposes the following examples differ slightly from the template implementation in this repo
### Creating a Nexa Wallet

[`openOrNewWallet()`](https://nexa.gitlab.io/libnexakotlin/libnexakotlin/org.nexa.libnexakotlin/open-or-new-wallet.html?query=fun%20openOrNewWallet(name:%20String,%20cs:%20ChainSelector):%20Bip44Wallet) - Open this wallet if it exists, or create a new HD BIP44 wallet

[`Bip44Wallet`](https://nexa.gitlab.io/libnexakotlin/libnexakotlin/org.nexa.libnexakotlin/-bip44-wallet/index.html?query=class%20Bip44Wallet(val%20name:%20String,%20val%20chainSelector:%20ChainSelector,%20var%20wdb:%20WalletDatabase)%20:%20CommonWallet) - The wallet class

```kotlin
val dbPrefix = "nexa_wallet"
val wallet: Bip44Wallet = openOrNewWallet(dbPrefix + "wal", ChainSelector.NEXA)
```

### Geting a new Nexa address string

[`getNewAddress()`](https://nexa.gitlab.io/libnexakotlin/libnexakotlin/org.nexa.libnexakotlin/-wallet/get-new-address.html?query=fun%20getNewAddress():%20PayAddress) - get a new nexa address from the wallet.

```kotlin
val address: String = wallet.getNewAddress().toString()
print(address)
nexa:nqtsq5g55t9699mcue00frjqql5275r3et45c3dqtxzfz8ru // Example address
```

### Sending $NEX

[`PayAddress`](https://nexa.gitlab.io/libnexakotlin/libnexakotlin/org.nexa.libnexakotlin/-pay-address/index.html?query=data%20class%20PayAddress(var%20blockchain:%20ChainSelector,%20var%20type:%20PayAddressType,%20var%20data:%20ByteArray)%20:%20BCHserializable) - Nexa address

[`BigDecimal`](https://github.com/ionspin/kotlin-multiplatform-bignum) - External implementation

[`toCurrency`](https://github.com/ionspin/kotlin-multiplatform-bignum) - Get this string as a BigDecimal currency value (using your default locale's number representation)

[`wallet.send(...)`](https://nexa.gitlab.io/libnexakotlin/libnexakotlin/org.nexa.libnexakotlin/-wallet/send.html) - Send funds to this destination. This function will select input coins from the wallet to fill the passed quantity

```kotlin
val spendAll = false
val sendAddress = PayAddress("nexa:nqtsq5g55t9699mcue00frjqql5275r3et45c3dqtxzfz8ru")
val amount: BigDecimal = "1000".toCurrency(chainSelector)
// Convert the default display units to the finest granularity of this currency. For example; MNEX to NEX.
val atomAmt: Long = Util.toFinestUnit(amount, chainSelector)
val transaction: iTransaction = wallet.send(atomAmt, sendAddress, deductFeeFromAmount = spendAll, sync = false, note = null)
println("Sending TX: ${transaction.toHex()}")
```

### Observing balance

[`wallet.setOnWalletChange(...)`](https://nexa.gitlab.io/libnexakotlin/libnexakotlin/org.nexa.libnexakotlin/-common-wallet/set-on-wallet-change.html) - Install a callback handler for whenever this wallet changes

[`wallet.balance`](https://nexa.gitlab.io/libnexakotlin/libnexakotlin/org.nexa.libnexakotlin/-wallet/balance.html) - the sum of the amounts in all confirmed unspent outputs spendable by this wallet

```kotlin
private var balance: Long = 0L

wallet.setOnWalletChange { wallet: Wallet ->
    // TODO: Display with correct decimal
    balance = wallet.balance
}
```

For an example of how to implement this and display the balance in Android Compose using a `ViewModel`, see `MainViewModel.kt`
