package org.bitcoinunlimited.nexaandroidtemplate

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.lifecycleScope
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import com.ionspin.kotlin.bignum.decimal.toBigDecimal
import kotlinx.coroutines.launch
import org.nexa.libnexakotlin.ChainSelector
import org.nexa.libnexakotlin.CurrencyDecimal
import org.nexa.libnexakotlin.SATperNEX
import org.nexa.libnexakotlin.SATperUBCH
import org.nexa.libnexakotlin.toLong

//? Convert the finest granularity of this currency to the default display unit.  For example, Satoshis to mBCH
fun fromFinestUnit(amount: Long, chainSelector: ChainSelector): BigDecimal
{
    val factor = when (chainSelector)
    {
        ChainSelector.NEXA, ChainSelector.NEXAREGTEST, ChainSelector.NEXATESTNET -> SATperNEX
        ChainSelector.BCH, ChainSelector.BCHREGTEST, ChainSelector.BCHTESTNET -> SATperUBCH
    }
    val ret = CurrencyDecimal(amount) / factor.toBigDecimal()
    return ret
}

val chainSelector: ChainSelector = ChainSelector.NEXA

class MainActivity: AppCompatActivity() {
    private val viewModel = MainViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            val balance = viewModel.balance.collectAsState()
            MaterialTheme {
                Surface(modifier = Modifier.fillMaxSize()) {
                    Column(
                        modifier = Modifier.padding(32.dp)
                    ) {
                        Text(text = "Balance: ${balance.value} NEX")
                        Spacer(modifier = Modifier.height(16.dp))
                        SendView(viewModel)
                        Spacer(modifier = Modifier.height(16.dp))
                        ClickToCopyText(viewModel.newAddress())
                    }
                }
            }
        }

        lifecycleScope.launch {
            viewModel.toastMessage.collect { message ->
                message?.let {
                    Toast.makeText(this@MainActivity, it, Toast.LENGTH_LONG).show()
                    viewModel.resetToast() // Reset message to avoid duplicate toasts
                }
            }
        }
    }
}

@Composable
fun ClickToCopyText(textToCopy: String) {
    val context = LocalContext.current
    val clipboardManager = LocalClipboardManager.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .clickable {
                clipboardManager.setText(androidx.compose.ui.text.AnnotatedString(textToCopy))
                Toast
                    .makeText(context, "Text copied to clipboard", Toast.LENGTH_LONG)
                    .show()
            },
    ) {
        Text(
            text ="Click anywhere to copy the Nexa address to your clipboard",
            fontSize = 20.sp
        )
        Spacer(modifier = Modifier.height(16.dp))
        Text(
            text = textToCopy,
            fontSize = 20.sp
        )
    }
}

@Composable
fun SendView(viewModel: MainViewModel) {
    var nexToSend by remember { mutableStateOf("") }
    var sendAddress by remember { mutableStateOf("") }

    fun isValidDecimalInput(input: String): Boolean {
        // Allows any number of digits before the decimal point, an optional decimal point, and up to two digits after the decimal point.
        return input.isEmpty() || input.matches(Regex("^\\d*\\.?\\d{0,2}$"))
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        TextField(
            value = nexToSend,
            onValueChange = {
                if (isValidDecimalInput(it)) {
                    nexToSend = it
                }
            },
            label = { Text("NEX") },
            modifier = Modifier.fillMaxWidth()
        )
        TextField(
            value = sendAddress,
            onValueChange = {
                sendAddress = it
            },
            label = { Text("Address") },
            modifier = Modifier.fillMaxWidth()
        )
        Button(
            modifier = Modifier.fillMaxWidth(),
            onClick = {
                viewModel.send(sendAddress, nexToSend, viewModel)
        }) {
            Text(text = "Send")
        }
    }
}

object Util {
    /** Convert the default display units to the finest granularity of this currency.  For example, MNEX to NEX */
    fun toFinestUnit(amount: BigDecimal, chainSelector: ChainSelector): Long
    {
        val ret:Long = when (chainSelector)
        {
            ChainSelector.NEXA, ChainSelector.NEXAREGTEST, ChainSelector.NEXATESTNET ->
                (amount* CurrencyDecimal(SATperNEX)).toLong()

            ChainSelector.BCH, ChainSelector.BCHREGTEST, ChainSelector.BCHTESTNET -> (amount* CurrencyDecimal(
                SATperUBCH
            )).toLong()
        }
        return ret
    }
}

