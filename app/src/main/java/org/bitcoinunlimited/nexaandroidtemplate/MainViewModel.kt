package org.bitcoinunlimited.nexaandroidtemplate

import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ionspin.kotlin.bignum.decimal.BigDecimal
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import org.nexa.libnexakotlin.ChainSelector
import org.nexa.libnexakotlin.PayAddress
import org.nexa.libnexakotlin.WalletNotEnoughBalanceException
import org.nexa.libnexakotlin.handleThreadException
import org.nexa.libnexakotlin.iTransaction
import org.nexa.libnexakotlin.openOrNewWallet
import org.nexa.libnexakotlin.toCurrency

class MainViewModel(): ViewModel() {
    private val dbPrefix = "nexa_wallet"
    private val wallet = openOrNewWallet(dbPrefix + "wal", chainSelector)
    private val _balance = MutableStateFlow<Long>(0)
    val balance = _balance.asStateFlow()
    private val _toastMessage = MutableStateFlow<String?>(null)
    val toastMessage = _toastMessage.asStateFlow()
    init {
        observeBalance()
    }

    private fun observeBalance()
    {
        wallet.setOnWalletChange {
            // TODO: Display with correct decimal
            _balance.value = it.balance
        }
    }

    fun showToast(message: String) {
        _toastMessage.value = message
    }

    fun resetToast() {
        _toastMessage.value = null
    }

    fun newAddress(): String {
        return wallet.getNewAddress().toString()
    }

    fun send(address: String, qty: String, viewModel: MainViewModel, spendAll: Boolean = false)
    {
        val balance = viewModel.balance.value
        viewModelScope.launch(Dispatchers.IO) {
            val sendAddress = PayAddress(address)
            val amount: BigDecimal = qty.toCurrency(chainSelector)

            if ((amount == BigDecimal.ZERO))  // Sending nothing
            {
                throw Exception("Amount is BigDecimal.ZERO")
            }
            else
            {
                try
                {
                    val atomAmt = Util.toFinestUnit(amount, chainSelector)
                    // If we are spending all, then deduct the fee from the amount (which was set above to the full ungrouped balance)
                    val transaction = wallet.send(atomAmt, sendAddress, deductFeeFromAmount = spendAll, sync = false, note = null)
                    println("Sending TX: ${transaction}")
                    println("Sending TX.toHex: ${transaction.toHex()}")
                    println("Sending TX.chainSelector: ${transaction.chainSelector}")
                    println("Sending TX.fee: ${transaction.fee}")
                    showToast("Sending TX.toHex: ${transaction.toHex()} .chainSelector: ${transaction.chainSelector} .fee: ${transaction.fee}")
                }
                catch (e: WalletNotEnoughBalanceException)  // We don't want to crash, we want to tell the user what went wrong
                {
                    handleThreadException(e)
                    println(e)
                    showToast("Not enough NEX balance")
                }
                catch (e: Exception)  // We don't want to crash, we want to tell the user what went wrong
                {
                    handleThreadException(e)
                    println(e)
                    showToast(e.toString())
                }
            }
        }
    }

}

