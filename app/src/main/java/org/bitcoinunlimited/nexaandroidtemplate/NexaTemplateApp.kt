package org.bitcoinunlimited.nexaandroidtemplate

import android.app.Application
import org.nexa.libnexakotlin.initializeLibNexa

@OptIn(ExperimentalUnsignedTypes::class)
class NexaTemplateApp: Application() {
    init {
        initializeLibNexa()
    }
}