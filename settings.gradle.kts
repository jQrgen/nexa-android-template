pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven { url = uri("https://gitlab.com/api/v4/projects/48545045/packages/maven") } // LibNexaKotlin
        maven { url = uri("https://gitlab.com/api/v4/projects/48544966/packages/maven") } // mpthreads
    }
}

rootProject.name = "Nexa Android Template"
include(":app")
